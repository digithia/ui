import { Configuration } from 'webpack'
import path from 'path'
import TsconfigPathsPlugin from 'tsconfig-paths-webpack-plugin'

const config: Configuration = {
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: [
          {
            loader: 'ts-loader',
            options: {
              configFile: 'tsconfig.json',
            },
          },
        ],
        exclude: /node_modules/,
      },
      {
        test: /scss$/,
        use: [
          'lit-css-loader',
          'css-modules-typescript-loader',
          {
            loader: 'postcss-loader',
            options: {
              postcssOptions: {
                plugins: [
                  require('postcss-import'),
                  require('autoprefixer'),
                  require('cssnano'),
                ],
              },
            },
          },
          'sass-loader',
        ],
      },
      {
        test: /\.svg$/,
        loader: 'svg-inline-loader',
      },
    ],
  },
  target: 'web',
  resolve: {
    plugins: [new TsconfigPathsPlugin({})],
    modules: ['node_modules'],
    extensions: ['.ts', '.js', '.scss'],
    alias: {
      '@src': path.resolve(__dirname, 'src/'),
    },
  },
}

export default config
