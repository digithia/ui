import * as webpack from 'webpack'
import { Configuration } from 'webpack'
import path from 'path'
import CopyPlugin from 'copy-webpack-plugin'
import { CleanWebpackPlugin } from 'clean-webpack-plugin'
import { merge } from 'webpack-merge'
import commonConfig from './webpack-common.config'

const config: Configuration = merge(commonConfig, {
  mode: 'production',
  entry: {
    index: ['./vendors.js', path.resolve(__dirname, './src/index.ts')],
    'badges/index': [
      './vendors.js',
      path.resolve(__dirname, './src/badges/index.ts'),
    ],
    'buttons/index': [
      './vendors.js',
      path.resolve(__dirname, './src/buttons/index.ts'),
    ],
    'cards/index': [
      './vendors.js',
      path.resolve(__dirname, './src/cards/index.ts'),
    ],
    'collapsible/index': [
      './vendors.js',
      path.resolve(__dirname, './src/collapsible/index.ts'),
    ],
    'icons/index': [
      './vendors.js',
      path.resolve(__dirname, './src/icons/index.ts'),
    ],
    'loaders/index': [
      './vendors.js',
      path.resolve(__dirname, './src/loaders/index.ts'),
    ],
    'nav/index': [
      './vendors.js',
      path.resolve(__dirname, './src/nav/index.ts'),
    ],
    'pagination/index': [
      './vendors.js',
      path.resolve(__dirname, './src/pagination/index.ts'),
    ],
    'popup/index': [
      './vendors.js',
      path.resolve(__dirname, './src/popup/index.ts'),
    ],
    'tabs/index': [
      './vendors.js',
      path.resolve(__dirname, './src/tabs/index.ts'),
    ],
    'titles/index': [
      './vendors.js',
      path.resolve(__dirname, './src/titles/index.ts'),
    ],
    'toasts/index': [
      './vendors.js',
      path.resolve(__dirname, './src/toasts/index.ts'),
    ],
  },
  externals: {
    './vendors.js': './vendors.js',
    '../vendors.js': '../vendors.js',
  },
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: '[name].js',
    library: 'digithiaUi',
    libraryTarget: 'umd',
    umdNamedDefine: true,
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        lit: {
          test: /node_modules\/lit-*/,
          name: 'vendors',
          chunks: 'all',
          reuseExistingChunk: true,
          enforce: true,
        },
      },
    },
  },
  plugins: [
    new CleanWebpackPlugin(),
    new CopyPlugin({
      patterns: [
        {
          from: path.resolve(__dirname, './package.json'),
          to: path.resolve(__dirname, './dist/package.json'),
        },
        {
          from: path.resolve(__dirname, './README.md'),
          to: path.resolve(__dirname, './dist/README.md'),
        },
      ],
    }),
  ],
})

export default config
