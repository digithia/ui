import {
  customElement,
  LitElement,
  property,
  css,
  html,
  CSSResult,
} from 'lit-element'

import sharedStyle from '@src/dig-ui-shared.scss'
import style from './dig-ui-loader.scss'

@customElement('dig-ui-loader')
export class DigUiLoader extends LitElement {
  static styles = css`
    ${sharedStyle as CSSResult}
    ${style as CSSResult}
  `

  @property({ type: String, attribute: 'loader-color' })
  loaderColor = `var(--dig-ui-loader-border-color)`

  render() {
    return html`
      <div
        class="dig-ui-loader"
        style="border-left-color: ${this.loaderColor};
          border-top-color: ${this.loaderColor};
          border-bottom-color: ${this.loaderColor}"
      ></div>
    `
  }
}
