export abstract class UiComponent {
  public wrapperEl: HTMLElement

  constructor(wrapperElement: HTMLElement | null) {
    if (!wrapperElement) {
      throw new Error(`Invalid HTML element provided, got ${wrapperElement}`)
    }
    this.wrapperEl = wrapperElement
  }

  protected abstract createComponent(): void
}
