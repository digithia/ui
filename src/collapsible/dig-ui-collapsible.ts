import {
  customElement,
  LitElement,
  css,
  property,
  html,
  CSSResult,
} from 'lit-element'
import { classMap } from 'lit-html/directives/class-map'

import sharedStyle from '@src/dig-ui-shared.scss'
import style from './dig-ui-collapsible.scss'

@customElement('dig-ui-collapsible')
export class DigUiCollapsible extends LitElement {
  static styles = css`
    ${sharedStyle as CSSResult}
    ${style as CSSResult}
  `

  @property({ type: String })
  title = ''

  @property({ type: Boolean })
  hidden = true

  toggleHidden() {
    this.hidden = !this.hidden
  }

  render() {
    return html` <div
        class="dig-ui-collapsible-title ${classMap({ hidden: this.hidden })}"
        @click="${this.toggleHidden}"
      >
        ${this.title}
      </div>
      <div
        class="dig-ui-collapsible-content ${classMap({ hidden: this.hidden })}"
      >
        <slot></slot>
      </div>`
  }
}
