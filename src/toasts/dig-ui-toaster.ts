import {
  customElement,
  LitElement,
  css,
  html,
  CSSResult,
  property,
} from 'lit-element'

import sharedStyle from '@src/dig-ui-shared.scss'
import style from './dig-ui-toaster.scss'

@customElement('dig-ui-toaster')
export class DigUiToaster extends LitElement {
  static styles = css`
    ${sharedStyle as CSSResult}
    ${style as CSSResult}
  `

  @property({ type: Boolean })
  right: boolean = false
  @property({ type: Boolean })
  left: boolean = false
  @property({ type: Boolean })
  top: boolean = false
  @property({ type: Boolean })
  bottom: boolean = false

  render() {
    return html`<slot></slot>`
  }
}
