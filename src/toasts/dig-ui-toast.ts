import {
  customElement,
  LitElement,
  css,
  property,
  html,
  internalProperty,
  CSSResult,
  query,
} from 'lit-element'
import { unsafeSVG } from 'lit-html/directives/unsafe-svg'
import { classMap } from 'lit-html/directives/class-map'
import { DigUtils } from '@digithia/util'

import sharedStyle from '@src/dig-ui-shared.scss'
import style from './dig-ui-toast.scss'

@customElement('dig-ui-toast')
export class DigUiToast extends LitElement {
  static styles = css`
    ${sharedStyle as CSSResult}
    ${style as CSSResult}
  `

  @property({ type: String })
  type = 'success'
  @property({ type: Number })
  lifespan = 5000

  @internalProperty()
  disappear = false

  timer!: NodeJS.Timeout

  get svgTag() {
    const svgString = require(`@src/assets/${this.type}.svg`)
    return html`${unsafeSVG(svgString)}`
  }

  constructor() {
    super()
    this.setTimeout()
    this.addEventListener('mouseenter', this.clearTimeout)
    this.addEventListener('mouseleave', this.resetTimeout)
    this.addEventListener('click', this.destroy)
  }

  render() {
    return html`
      <div
        class=${classMap({
          'dig-ui-toast': true,
          [this.type]: true,
          disappear: this.disappear,
        })}
      >
        <div class="svg-container">${this.svgTag}</div>
        <slot></slot>
      </div>
    `
  }

  attributeChangedCallback(name: string, oldval: string, newval: string) {
    super.attributeChangedCallback(name, oldval, newval)
    if (name === 'lifespan') {
      this.resetTimeout()
    }
  }

  destroy = () => {
    this.disappear = true

    const animationDuration = getComputedStyle(this).getPropertyValue(
      '--dig-ui-toast-disappear-duration'
    )
    this.clearTimeout()

    setTimeout(() => {
      this.remove()
    }, DigUtils.cssTimeToMilliseconds(animationDuration))
  }

  clearTimeout = () => {
    clearTimeout(this.timer)
  }

  setTimeout = () => {
    this.timer = setTimeout(() => {
      this.destroy()
    }, this.lifespan)
  }

  resetTimeout = () => {
    this.clearTimeout()
    this.setTimeout()
  }
}
