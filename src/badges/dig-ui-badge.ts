import {
  customElement,
  LitElement,
  property,
  css,
  html,
  CSSResult,
} from 'lit-element'

import sharedStyle from '@src/dig-ui-shared.scss'
import style from './dig-ui-badge.scss'

@customElement('dig-ui-badge')
export class DigUiBadge extends LitElement {
  static styles = css`
    ${sharedStyle as CSSResult}
    ${style as CSSResult}
  `

  @property({ type: String, attribute: 'bg-color' })
  bgColor = `var(--dig-ui-badge-background-color)`

  @property({ type: String, attribute: 'text-color' })
  textColor = `var(--dig-ui-badge-text-color)`

  render() {
    return html` <div
      class="dig-ui-badge"
      style="background-color: '${this.bgColor}'; color: '${this.textColor}';"
    >
      <slot></slot>
    </div>`
  }
}
