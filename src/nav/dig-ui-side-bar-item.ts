import {
  customElement,
  LitElement,
  css,
  html,
  property,
  CSSResult,
} from 'lit-element'
import { classMap } from 'lit-html/directives/class-map'

import sharedStyle from '@src/dig-ui-shared.scss'
import style from './dig-ui-side-bar-item.scss'

@customElement('dig-ui-side-bar-item')
export class DigUiSideBarItem extends LitElement {
  static styles = css`
    ${sharedStyle as CSSResult}
    ${style as CSSResult}
  `

  render() {
    return html`
      <div class="${classMap({ item: true })}" @click="${this.toggle}">
        <slot></slot>
      </div>
    `
  }

  toggle() {
    this.dispatchEvent(new CustomEvent('toggle', { bubbles: true }))
  }

  attributeChangedCallback(name: string, oldval: string, newval: string) {
    super.attributeChangedCallback(name, oldval, newval)
  }
}
