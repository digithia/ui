import { customElement, LitElement, css, html, CSSResult } from 'lit-element'

import sharedStyle from '@src/dig-ui-shared.scss'
import style from './dig-ui-nav-bar-item.scss'

@customElement('dig-ui-nav-bar-item')
export class DigUiNavBarItem extends LitElement {
  static styles = css`
    ${sharedStyle as CSSResult}
    ${style as CSSResult}
  `

  render() {
    return html`
      <div class="item">
        <slot></slot>
      </div>
    `
  }
}
