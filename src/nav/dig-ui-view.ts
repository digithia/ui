import {
  customElement,
  LitElement,
  css,
  html,
  property,
  CSSResult,
  query,
} from 'lit-element'
import { classMap } from 'lit-html/directives/class-map'

import sharedStyle from '@src/dig-ui-shared.scss'
import style from './dig-ui-view.scss'

@customElement('dig-ui-view')
export class DigUiView extends LitElement {
  static styles = css`
    ${sharedStyle as CSSResult}
    ${style as CSSResult}
  `

  @property({ type: Boolean, reflect: true })
  toggled = false

  @property({ type: Boolean, attribute: 'close-menu' })
  closeMenu = false

  @query('#dig-ui-side-bar')
  slotNavSideEl!: HTMLSlotElement

  constructor() {
    super()
    window.addEventListener('resize', this.resize)
  }

  render() {
    return html`
      <div class="nav-bar">
        ${this.closeMenu
          ? html` <dig-ui-nav-bar-item
              slot="nav-bar"
              class="dig-ui-nav-bar-item-menu-button"
              @click="${() => {
                this.toggle()
              }}"
            >
              <dig-ui-menu-button
                ?toggled="${this.toggled}"
              ></dig-ui-menu-button>
            </dig-ui-nav-bar-item>`
          : ``}
        <slot name="nav-bar"></slot>
      </div>
      <div class="main">
        <div class="${classMap({ 'side-bar': true, toggled: this.toggled })}">
          <slot
            id="dig-ui-side-bar"
            name="side-bar"
            @slotchange="${this.slotChangeSideBar}"
            @toggle="${this.closeMobileSideBar}"
          ></slot>
        </div>
        <div class="content" @click="${this.closeMobileSideBar}">
          <div class="filter"></div>
          <slot name="content"></slot>
          <slot name="footer"></slot>
        </div>
      </div>
    `
  }

  slotChangeSideBar(e: any) {
    this.resize()
    this.refreshSideBar()
  }

  refreshSideBar() {
    const navBar = this.shadowRoot?.querySelector('.nav-bar') as HTMLElement
    const sideBar = this.shadowRoot?.querySelector('.side-bar') as HTMLElement
    const navBarHeight = navBar?.clientHeight
    const sideBarWidth = sideBar?.clientWidth
    this.style.setProperty('--dig-ui-nav-bar-height', navBarHeight + 1 + 'px')
    this.style.setProperty('--dig-ui-side-bar-width', sideBarWidth + 1 + 'px')
  }

  toggle() {
    this.toggled = !this.toggled
  }

  resize = () => {
    const width = this.shadowRoot?.host.clientWidth
    if (width && width <= 600 && this.toggled) {
      this.toggled = false
      this.dispatchEvent(new CustomEvent('toggle', { detail: false }))
    } else if (width && width > 600 && !this.toggled) {
      this.toggled = true
      this.dispatchEvent(new CustomEvent('toggle', { detail: true }))
    }
  }

  closeMobileSideBar() {
    const width = this.shadowRoot?.host.clientWidth

    if (width && width <= 600 && this.toggled) {
      this.toggled = false
      this.dispatchEvent(new CustomEvent('toggle', { detail: false }))
    }
  }
}
