import {
  customElement,
  LitElement,
  css,
  html,
  CSSResult,
  property,
} from 'lit-element'
import { classMap } from 'lit-html/directives/class-map'

import sharedStyle from '@src/dig-ui-shared.scss'
import style from './dig-ui-popup.scss'

@customElement('dig-ui-popup')
export class DigUiPopup extends LitElement {
  static styles = css`
    ${sharedStyle as CSSResult}
    ${style as CSSResult}
  `

  @property({ type: Boolean })
  required = false
  @property({ type: Boolean })
  toggled = false

  render() {
    return html`
      <div class="dig-ui-popup ${classMap({
            toggled: this.toggled,
            required: this.required,
          })}" @click="${this.clickOutside}">
        <div class="dig-ui-popup-card">
          <slot></slot>
        </div>
      </div>
    `
  }

  trigger() {
    this.toggled = !this.toggled
  }

  clickOutside = (event: MouseEvent) => {
    if (this.required) return
    if ((event.target as HTMLElement).classList.contains('dig-ui-popup')) {
      this.close()
    }
  }

  close() {
    this.toggled = false
    this.dispatchEvent(
      new CustomEvent('close')
    )
  }
}
