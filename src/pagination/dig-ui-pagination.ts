import {
  customElement,
  LitElement,
  css,
  property,
  html,
  CSSResult,
  internalProperty,
} from 'lit-element'
import { classMap } from 'lit-html/directives/class-map'

import sharedStyle from '@src/dig-ui-shared.scss'
import style from './dig-ui-pagination.scss'

@customElement('dig-ui-pagination')
export class DigUiPagination extends LitElement {
  static styles = css`
    ${sharedStyle as CSSResult}
    ${style as CSSResult}
  `

  @property({ type: Number, attribute: 'current-page' })
  currentPage = 1
  @property({ type: Number, attribute: 'page-count' })
  pageCount = 2
  @property({ type: Number, attribute: 'end-page-count' })
  endPageCount = 2
  @property({ type: Number, attribute: 'neighbour-page-count' })
  neighbourPageCount = 2
  @property({ type: String, attribute: 'previous-text' })
  previousText = 'Prev'
  @property({ type: String, attribute: 'next-text' })
  nextText = 'Next'

  @internalProperty()
  pages: (number | string)[] = []

  render() {
    return html`
      <div class="dig-ui-pagination">
        <div class="dig-ui-pagination-pages">
          <div class="dig-ui-pagination-page" @click="${this.previous}">
            ${this.previousText}
          </div>
          ${this.pages.map((page) => {
            return html`<div
              class="dig-ui-pagination-page ${classMap({
                selected: page === this.currentPage,
              })}"
              @click="${() => this.select(page)}"
            >
              <span>${page}</span>
            </div>`
          })}
          <div class="dig-ui-pagination-page" @click="${this.next}">
            ${this.nextText}
          </div>
        </div>
      </div>
    `
  }

  firstUpdated() {
    this.createPages(this.pageCount)
  }

  attributeChangedCallback(name: string, oldval: string, newval: string): void {
    super.attributeChangedCallback(name, oldval, newval)
    if (name === 'page-count') {
      this.createPages(+newval)
    } else if (name === 'current-page') {
      this.select(this.currentPage)
    }
  }

  isFirstPart(index: number, total: number): boolean {
    return index <= total - (this.neighbourPageCount + this.endPageCount + 1)
  }

  isLastPart(index: number, total: number): boolean {
    return index >= total - (this.neighbourPageCount + this.endPageCount + 1)
  }

  createPages(count: number) {
    this.pages = []
    const displayedPageCount =
      2 * (this.neighbourPageCount + this.endPageCount + 1) + 1

    if (count <= displayedPageCount) {
      for (let i = 1; i <= count; i++) {
        this.pages.push(i)
      }
    } else {
      for (let i = 1; i <= this.endPageCount; i++) {
        this.pages.push(i)
      }

      if (this.isFirstPart(this.currentPage, displayedPageCount)) {
        for (
          let i = this.endPageCount + 1;
          i <= displayedPageCount - (this.endPageCount + 1);
          i++
        ) {
          this.pages.push(i)
        }
        this.pages.push('>')
      } else if (this.isLastPart(this.currentPage, count)) {
        this.pages.push('<')
        for (
          let i = count - displayedPageCount + this.endPageCount + 2;
          i <= count - this.endPageCount;
          i++
        ) {
          this.pages.push(i)
        }
      } else {
        this.pages.push('<')
        for (
          let i = -this.neighbourPageCount;
          i <= this.neighbourPageCount;
          i++
        ) {
          this.pages.push(this.currentPage + i)
        }
        this.pages.push('>')
      }

      for (let i = count - this.endPageCount + 1; i <= count; i++) {
        this.pages.push(i)
      }
    }
  }

  setCurrentPage(index: number) {
    if (index > this.pageCount) {
      this.currentPage = this.pageCount
      this.dispatchEvent(
        new CustomEvent('change', { detail: this.currentPage })
      )
    } else if (index < 1) {
      this.currentPage = 1
      this.dispatchEvent(
        new CustomEvent('change', { detail: this.currentPage })
      )
    } else if (index !== this.currentPage) {
      this.currentPage = index
      this.dispatchEvent(
        new CustomEvent('change', { detail: this.currentPage })
      )
    }
  }

  previous() {
    this.setCurrentPage(this.currentPage - 1)
    this.createPages(this.pageCount)
  }

  next() {
    this.setCurrentPage(this.currentPage + 1)
    this.createPages(this.pageCount)
  }

  select(index: number | string) {
    if (typeof index === 'string') {
      if (index === '>') {
        this.setCurrentPage(this.currentPage + 5)
      } else if (index === '<') {
        this.setCurrentPage(this.currentPage - 5)
      }
    } else {
      this.setCurrentPage(index)
    }
    this.createPages(this.pageCount)
  }
}
