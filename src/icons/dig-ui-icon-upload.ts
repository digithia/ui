import {
  customElement,
  css,
  svg,
  CSSResult,
} from 'lit-element'
import { classMap } from 'lit-html/directives/class-map'

import sharedStyle from '@src/dig-ui-shared.scss'
import style from './dig-ui-icon-upload.scss'
import { DigUiIconButton } from './Icon'

@customElement('dig-ui-icon-upload')
export class DigUiIconUpload extends DigUiIconButton {
  static styles = css`
    ${sharedStyle as CSSResult}
    ${style as CSSResult}
  `

  render() {
    return svg`
      <svg class="upload ${classMap({
      animation: this.animation,
      focused: this.focused,
      hovered: this.hovered,
    })}"  version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 120 120">
        <defs>
          <clipPath id="clip-body">
            <polygon points="0,0 0,120 120,120 120,0 80,0 80,60 40,60 40,0" />
          </clipPath>
        </defs>

        <g class="body" stroke="grey" stroke-width="6" fill="white">
          <rect x="15" y="55" width="90" height="50" rx="12" clip-path="url(#clip-body)" />
        </g>

        <g class="arrow" fill="grey">
          <rect x="57" y="15" width="6" height="40" rx="3" transform="rotate(40, 60, 18)" />
          <rect x="57" y="15" width="6" height="40" rx="3" transform="rotate(-40, 60, 18)" />

          <rect x="35" y="42" width="15" height="6" rx="3"  />
          <rect x="70" y="42" width="15" height="6" rx="3"  />

          <rect x="44" y="42" width="6" height="50" rx="3"  />
          <rect x="70" y="42" width="6" height="50" rx="3"  />

          <rect x="44" y="87" width="32" height="6" rx="3"  />
        </g>
      </svg>
    `
  }
}
