import {
  customElement,
  css,
  svg,
  CSSResult,
} from 'lit-element'
import { classMap } from 'lit-html/directives/class-map'

import sharedStyle from '@src/dig-ui-shared.scss'
import style from './dig-ui-icon-moon.scss'
import { DigUiIconButton } from './Icon'

@customElement('dig-ui-icon-moon')
export class DigUiIconMoon extends DigUiIconButton {
  static styles = css`
    ${sharedStyle as CSSResult}
    ${style as CSSResult}
  `

  render() {
    return svg`
      <svg class="moon ${classMap({
      animation: this.animation,
      focused: this.focused,
      hovered: this.hovered,
    })}"  version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 120 120" fill="grey">
        <defs>
          <clipPath id="clip-moon">
            <circle cx="60" cy="60" r="44" />
          </clipPath>
          <clipPath id="clip-moon-2">
            <circle cx="60" cy="60" r="48" />
          </clipPath>
        </defs>

        <g class="moon-draw">
          <circle cx="60" cy="60" r="44" fill="grey" class="fill" />
          <circle cx="60" cy="60" r="38" fill="white" class="clip" />
          <circle cx="30" cy="60" r="44" fill="grey" class="fill" clip-path="url(#clip-moon)" />
          <circle cx="30" cy="60" r="38" fill="white" class="clip" clip-path="url(#clip-moon-2)" />
        </g>
      </svg>
    `
  }
}
