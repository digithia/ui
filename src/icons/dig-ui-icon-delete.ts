import {
  customElement,
  css,
  svg,
  CSSResult,
} from 'lit-element'
import { classMap } from 'lit-html/directives/class-map'

import sharedStyle from '@src/dig-ui-shared.scss'
import style from './dig-ui-icon-delete.scss'
import { DigUiIconButton } from './Icon'

@customElement('dig-ui-icon-delete')
export class DigUiIconDelete extends DigUiIconButton {
  static styles = css`
    ${sharedStyle as CSSResult}
    ${style as CSSResult}
  `

  render() {
    return svg`
      <svg class="trash ${classMap({
      animation: this.animation,
      focused: this.focused,
      hovered: this.hovered,
    })}"  version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 120 120">
        <g class="trash-lid" fill="grey">
          <path d="M 21 38 C 21 8, 99 8, 99 38" stroke="grey" stroke-width="6" fill="transparent"/>
          <rect x="18" y="35" width="84" height="6"  rx="3" />
        </g>

        <g class="trash-body" fill="grey">
           <rect x="18" y="35" width="84" height="6" rx="3" />
           <rect x="31" y="104" width="58" height="6" rx="3" />

           <rect x="18" y="35" width="6" height="76" rx="3" transform="rotate(-10, 21, 38)" />
           <rect x="96" y="35" width="6" height="76" rx="3" transform="rotate(10, 99, 38)" />

           <rect x="43" y="50" width="6" height="44" rx="3" transform="rotate(-6, 46, 53)" />
           <rect x="71" y="50" width="6" height="44" rx="3" transform="rotate(6, 74, 53)" />
        </g>
      </svg>
    `
  }
}
