import {
  customElement,
  css,
  svg,
  CSSResult,
} from 'lit-element'
import { classMap } from 'lit-html/directives/class-map'

import sharedStyle from '@src/dig-ui-shared.scss'
import style from './dig-ui-icon-sun.scss'
import { DigUiIconButton } from './Icon'

@customElement('dig-ui-icon-sun')
export class DigUiIconSun extends DigUiIconButton {
  static styles = css`
    ${sharedStyle as CSSResult}
    ${style as CSSResult}
  `

  render() {
    return svg`
      <svg class="sun ${classMap({
      animation: this.animation,
      focused: this.focused,
      hovered: this.hovered,
    })}"  version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 120 120" fill="grey">
        <g class="sun-circle">
          <circle cx="60" cy="60" r="34" fill="grey" class="fill" />
          <circle cx="60" cy="60" r="28" fill="white" class="clip" />
        </g>

        <g class="sun-rays-long">
          <rect x="6" y="57" width="17" height="6" rx="3" transform="rotate(0, 60, 60)" />
          <rect x="6" y="57" width="17" height="6" rx="3" transform="rotate(90, 60, 60)" />
          <rect x="6" y="57" width="17" height="6" rx="3" transform="rotate(180, 60, 60)" />
          <rect x="6" y="57" width="17" height="6" rx="3" transform="rotate(270, 60, 60)" />
        </g>

        <g class="sun-rays-short">
          <rect x="10" y="57" width="12" height="6" rx="3" transform="rotate(45, 60, 60)" />
          <rect x="10" y="57" width="12" height="6" rx="3" transform="rotate(135, 60, 60)" />
          <rect x="10" y="57" width="12" height="6" rx="3" transform="rotate(225, 60, 60)" />
          <rect x="10" y="57" width="12" height="6" rx="3" transform="rotate(315, 60, 60)" />
        </g>
      </svg>
    `
  }
}
