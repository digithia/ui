import {
  customElement,
  css,
  html,
  CSSResult,
  LitElement,
  property,
} from 'lit-element'
import { classMap } from 'lit-html/directives/class-map'

import sharedStyle from '@src/dig-ui-shared.scss'
import style from './dig-ui-icon.scss'

import { IconType } from './Icon'
import './dig-ui-icon-add'
import './dig-ui-icon-close'
import './dig-ui-icon-copy'
import './dig-ui-icon-delete'
import './dig-ui-icon-download'
import './dig-ui-icon-edit'
import './dig-ui-icon-heart'
import './dig-ui-icon-letter'
import './dig-ui-icon-moon'
import './dig-ui-icon-show'
import './dig-ui-icon-hide'
import './dig-ui-icon-sun'
import './dig-ui-icon-upload'

@customElement('dig-ui-icon')
export class DigUiIcon extends LitElement {
  static styles = css`
    ${sharedStyle as CSSResult}
    ${style as CSSResult}
  `

  @property({ type: Boolean })
  animation = true
  @property({ type: Boolean })
  focused = false
  @property({ type: Boolean })
  hovered = false
  @property({ type: String })
  type = IconType.ADD

  render() {
    return html`
      <button
        class="dig-ui-icon ${classMap({
          animation: this.animation,
          [this.type?.toLowerCase()]: true,
        })}"
        @focus="${() => (this.focused = true)}"
        @blur="${() => (this.focused = false)}"
        @mouseenter="${() => (this.hovered = true)}"
        @mouseleave="${() => (this.hovered = false)}"
      >
        ${this.type?.toUpperCase() === IconType.ADD
          ? html`<dig-ui-icon-add
              .animation="${this.animation}"
              .focused="${this.focused}"
              .hovered="${this.hovered}"
            ></dig-ui-icon-add>`
          : this.type?.toUpperCase() === IconType.CLOSE
          ? html`<dig-ui-icon-close
              .animation="${this.animation}"
              .focused="${this.focused}"
              .hovered="${this.hovered}"
            ></dig-ui-icon-close>`
          : this.type?.toUpperCase() === IconType.COPY
          ? html`<dig-ui-icon-copy
              .animation="${this.animation}"
              .focused="${this.focused}"
              .hovered="${this.hovered}"
            ></dig-ui-icon-copy>`
          : this.type?.toUpperCase() === IconType.DELETE
          ? html`<dig-ui-icon-delete
              .animation="${this.animation}"
              .focused="${this.focused}"
              .hovered="${this.hovered}"
            ></dig-ui-icon-delete>`
          : this.type?.toUpperCase() === IconType.DOWNLOAD
          ? html`<dig-ui-icon-download
              .animation="${this.animation}"
              .focused="${this.focused}"
              .hovered="${this.hovered}"
            ></dig-ui-icon-download>`
          : this.type?.toUpperCase() === IconType.EDIT
          ? html`<dig-ui-icon-edit
              .animation="${this.animation}"
              .focused="${this.focused}"
              .hovered="${this.hovered}"
            ></dig-ui-icon-edit>`
          : this.type?.toUpperCase() === IconType.HEART
          ? html`<dig-ui-icon-heart
              .animation="${this.animation}"
              .focused="${this.focused}"
              .hovered="${this.hovered}"
            ></dig-ui-icon-heart>`
          : this.type?.toUpperCase() === IconType.LETTER
          ? html`<dig-ui-icon-letter
              .animation="${this.animation}"
              .focused="${this.focused}"
              .hovered="${this.hovered}"
            ></dig-ui-icon-letter>`
          : this.type?.toUpperCase() === IconType.SHOW
          ? html`<dig-ui-icon-show
              .animation="${this.animation}"
              .focused="${this.focused}"
              .hovered="${this.hovered}"
            ></dig-ui-icon-show>`
          : this.type?.toUpperCase() === IconType.HIDE
          ? html`<dig-ui-icon-hide
              .animation="${this.animation}"
              .focused="${this.focused}"
              .hovered="${this.hovered}"
            ></dig-ui-icon-hide>`
          : this.type?.toUpperCase() === IconType.SUN
          ? html`<dig-ui-icon-sun
              .animation="${this.animation}"
              .focused="${this.focused}"
              .hovered="${this.hovered}"
            ></dig-ui-icon-sun>`
          : this.type?.toUpperCase() === IconType.MOON
          ? html`<dig-ui-icon-moon
              .animation="${this.animation}"
              .focused="${this.focused}"
              .hovered="${this.hovered}"
            ></dig-ui-icon-moon>`
          : this.type?.toUpperCase() === IconType.UPLOAD
          ? html`<dig-ui-icon-upload
              .animation="${this.animation}"
              .focused="${this.focused}"
              .hovered="${this.hovered}"
            ></dig-ui-icon-upload>`
          : html``}
      </button>
    `
  }
}
