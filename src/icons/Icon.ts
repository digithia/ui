import { LitElement, property } from 'lit-element'

export enum IconType {
  ADD = 'ADD',
  CLOSE = 'CLOSE',
  COPY = 'COPY',
  DELETE = 'DELETE',
  DOWNLOAD = 'DOWNLOAD',
  EDIT = 'EDIT',
  HEART = 'HEART',
  LETTER = 'LETTER',
  SHOW = 'SHOW',
  HIDE = 'HIDE',
  UPLOAD = 'UPLOAD',
  SUN = 'SUN',
  MOON = 'MOON',
}

export abstract class DigUiIconButton extends LitElement {
  @property({ type: Boolean })
  animation = true
  @property({ type: Boolean })
  focused = false
  @property({ type: Boolean })
  hovered = false
}
