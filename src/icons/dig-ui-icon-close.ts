import {
  customElement,
  css,
  svg,
  CSSResult,
} from 'lit-element'
import { classMap } from 'lit-html/directives/class-map'

import sharedStyle from '@src/dig-ui-shared.scss'
import style from './dig-ui-icon-close.scss'
import { DigUiIconButton } from './Icon'

@customElement('dig-ui-icon-close')
export class DigUiIconClose extends DigUiIconButton {
  static styles = css`
    ${sharedStyle as CSSResult}
    ${style as CSSResult}
  `

  render() {
    return svg`
      <svg class="cross ${classMap({
      animation: this.animation,
      focused: this.focused,
      hovered: this.hovered,
    })}"  version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 120 120" fill="grey">
        <rect x="20" y="20" width="110" height="10" rx="5" transform="rotate(45, 25, 25)" />
        <rect x="20" y="90" width="110" height="10" rx="5" transform="rotate(-45, 25, 95)" />
      </svg>
    `
  }
}
