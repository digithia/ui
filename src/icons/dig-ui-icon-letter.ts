import {
  customElement,
  css,
  svg,
  CSSResult,
} from 'lit-element'
import { classMap } from 'lit-html/directives/class-map'

import sharedStyle from '@src/dig-ui-shared.scss'
import style from './dig-ui-icon-letter.scss'
import { DigUiIconButton } from './Icon'

@customElement('dig-ui-icon-letter')
export class DigUiIconLetter extends DigUiIconButton {
  static styles = css`
    ${sharedStyle as CSSResult}
    ${style as CSSResult}
  `

  render() {
    return svg`
      <svg class="letter ${classMap({
      animation: this.animation,
      focused: this.focused,
      hovered: this.hovered,
    })}"  version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 120 120">

        <g class="letter-body" fill="transparent" stroke="grey" stroke-width="6">
           <rect x="10" y="25" width="100" height="70" rx="12" />
        </g>
        <g class="letter-fold" fill="grey">
          <rect x="12" y="27" width="50" height="6" rx="3" transform="rotate(40, 15, 30)" />
          <rect x="60" y="27" width="50" height="6" rx="3" transform="rotate(-40, 105, 30)" />

          <rect x="10" y="90" width="65" height="6" rx="3" transform="rotate(-40, 10, 90)" />
          <rect x="45" y="90" width="65" height="6" rx="3" transform="rotate(40, 110, 90)" />
        </g>
        <g class="letter-head" fill="grey">
          <rect x="43" y="24" width="65" height="6" rx="3" transform="rotate(-40, 110, 27)" />
          <rect x="12" y="24" width="65" height="6" rx="3" transform="rotate(40, 10, 27)" />
        </g>
        <g class="letter-mask" fill="red">
          <polygon points="39,46 60,65 81,46" />
        </g>
      </svg>
    `
  }
}
