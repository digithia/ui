import {
  customElement,
  css,
  svg,
  CSSResult,
} from 'lit-element'
import { classMap } from 'lit-html/directives/class-map'

import sharedStyle from '@src/dig-ui-shared.scss'
import style from './dig-ui-icon-edit.scss'
import { DigUiIconButton } from './Icon'

@customElement('dig-ui-icon-edit')
export class DigUiIconEdit extends DigUiIconButton {
  static styles = css`
    ${sharedStyle as CSSResult}
    ${style as CSSResult}
  `

  render() {
    return svg`
      <svg class="pencil ${classMap({
      animation: this.animation,
      focused: this.focused,
      hovered: this.hovered,
    })}"  version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 120 120">
        <g transform="rotate(-45, 60, 60)">
          <g class="pencil-rubber" fill="white" stroke="grey">
            <rect x="100" y="50" width="18" height="20" stroke-width="6" rx="6"/>
          </g>
          <g class="pencil-body" fill="white" stroke="grey">
            <rect x="28" y="50" width="74" height="20" stroke-width="6" />
          </g>
          <g class="pencil-spike" fill="grey">
            <rect x="0" y="57" width="30" height="6" rx="3" transform="rotate(23, 3, 60)"  />
            <rect x="0" y="57" width="30" height="6" rx="3" transform="rotate(-23, 3, 60)"  />
          </g>
        </g>
      </svg>
    `
  }
}
