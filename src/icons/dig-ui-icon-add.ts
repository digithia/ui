import {
  customElement,
  css,
  svg,
  CSSResult,
} from 'lit-element'
import { classMap } from 'lit-html/directives/class-map'

import sharedStyle from '@src/dig-ui-shared.scss'
import style from './dig-ui-icon-add.scss'
import { DigUiIconButton } from './Icon'

@customElement('dig-ui-icon-add')
export class DigUiIconAdd extends DigUiIconButton {
  static styles = css`
    ${sharedStyle as CSSResult}
    ${style as CSSResult}
  `

  render() {
    return svg`
      <svg class="plus ${classMap({
      animation: this.animation,
      focused: this.focused,
      hovered: this.hovered,
    })}"  version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 120 120" fill="grey">
        <rect x="10" y="55" width="100" height="10" rx="5" />
        <rect x="55" y="10" width="10" height="100" rx="5" />
      </svg>
    `
  }
}
