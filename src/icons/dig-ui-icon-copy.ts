import {
  customElement,
  css,
  svg,
  CSSResult,
} from 'lit-element'
import { classMap } from 'lit-html/directives/class-map'

import sharedStyle from '@src/dig-ui-shared.scss'
import style from './dig-ui-icon-copy.scss'
import { DigUiIconButton } from './Icon'

@customElement('dig-ui-icon-copy')
export class DigUiIconCopy extends DigUiIconButton {
  static styles = css`
    ${sharedStyle as CSSResult}
    ${style as CSSResult}
  `

  render() {
    return svg`
      <svg class="copy ${classMap({
      animation: this.animation,
      focused: this.focused,
      hovered: this.hovered,
    })}"  version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 120 120">
        <g class="back" stroke="grey" stroke-width="6" fill="white">
          <rect x="18" y="18" width="70" height="70" rx="12" />
        </g>
        <g class="front" stroke="grey" stroke-width="6" fill="white">
          <rect x="32" y="32" width="70" height="70" rx="12" />
        </g>
      </svg>
    `
  }
}
