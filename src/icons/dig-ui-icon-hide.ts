import {
  customElement,
  css,
  svg,
  CSSResult,
} from 'lit-element'
import { classMap } from 'lit-html/directives/class-map'

import sharedStyle from '@src/dig-ui-shared.scss'
import style from './dig-ui-icon-hide.scss'
import { DigUiIconButton } from './Icon'

@customElement('dig-ui-icon-hide')
export class DigUiIconHide extends DigUiIconButton {
  static styles = css`
    ${sharedStyle as CSSResult}
    ${style as CSSResult}
  `

  render() {
    return svg`
      <svg class="eye ${classMap({
      animation: this.animation,
      focused: this.focused,
      hovered: this.hovered,
    })}"  version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 120 120">
        <defs>
          <clipPath id="clip-close">
            <ellipse cx="60" cy="60" rx="50" ry="30" />
          </clipPath>
        </defs>

        <g class="eye-outline" stroke="grey" stroke-width="6">
          <path d="M 10 61 C 30 20, 90 20, 110 61" fill="transparent"/>
          <path d="M 10 59 C 30 100, 90 100, 110 59" fill="transparent"/>
        </g>
        <g class="eye-iris" stroke-width="6" stroke="grey">
          <circle cx="60" cy="60" r="15" fill="transparent"/>
        </g>
        <g class="eye-pupil" fill="grey">
          <circle cx="60" cy="60" r="5" />
        </g>
        <g class="eye-close" fill="grey">
          <ellipse id="close" cx="60" cy="30" rx="60" ry="0" clip-path="url(#clip-close)"/>
        </g>

        <g class="eye-bar" fill="grey">
          <rect x="16" y="16" width="120" height="6" rx="3" transform="rotate(45, 19, 19)" />
        </g>
      </svg>
    `
  }
}
