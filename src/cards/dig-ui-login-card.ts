import { customElement, LitElement, css, html, CSSResult } from 'lit-element'

import sharedStyle from '@src/dig-ui-shared.scss'
import style from './dig-ui-login-card.scss'

@customElement('dig-ui-login-card')
export class DigUiLoginCard extends LitElement {
  static styles = css`
    ${sharedStyle as CSSResult}
    ${style as CSSResult}
  `

  render() {
    return html`
      <div class="login-card">
        <slot name="logo"></slot>
        <slot name="title"></slot>
        <slot name="form"></slot>
        <slot name="links"></slot>
      </div>
    `
  }
}
