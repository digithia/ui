import {
  customElement,
  LitElement,
  css,
  html,
  CSSResult,
  property,
} from 'lit-element'

import sharedStyle from '@src/dig-ui-shared.scss'
import style from './dig-ui-edit-card.scss'

@customElement('dig-ui-edit-card')
export class DigUiEditCard extends LitElement {
  static styles = css`
    ${sharedStyle as CSSResult}
    ${style as CSSResult}
  `

  @property({ type: Boolean, reflect: true })
  toggled = false

  render() {
    return html`
      <div class="edit-card">
        <div class="ctrl-buttons">
          ${!this.toggled
            ? html`
                <dig-ui-edit-button
                  @click="${() => (this.toggled = true)}"
                ></dig-ui-edit-button>
              `
            : html`
                <dig-ui-close-button
                  @click="${() => (this.toggled = false)}"
                ></dig-ui-close-button>
              `}
        </div>
        ${!this.toggled ? html`<slot name="content"></slot>` : ''}
        ${this.toggled ? html`<slot name="form"></slot>` : ''}
      </div>
    `
  }

  toggle(event: CustomEvent): void {
    this.toggled = event.detail
  }
}
