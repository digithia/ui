import {
  customElement,
  LitElement,
  css,
  property,
  html,
  CSSResult,
} from 'lit-element'
import { classMap } from 'lit-html/directives/class-map'

import sharedStyle from '@src/dig-ui-shared.scss'
import style from './dig-ui-title.scss'

@customElement('dig-ui-title')
export class DigUiTitle extends LitElement {
  static styles = css`
    ${sharedStyle as CSSResult}
    ${style as CSSResult}
  `

  @property({ type: Boolean })
  line = false

  render() {
    return html`
      <h1 class="title ${classMap({ line: this.line })}">
        <span class="text">
          <slot></slot>
        </span>
      </h1>
    `
  }
}
