import {
  customElement,
  LitElement,
  property,
  css,
  html,
  CSSResult,
} from 'lit-element'
import { classMap } from 'lit-html/directives/class-map'
import sharedStyle from '@src/dig-ui-shared.scss'
import style from './dig-ui-tab.scss'

@customElement('dig-ui-tab')
export class DigUiTab extends LitElement {
  static styles = css`
    ${sharedStyle as CSSResult}
    ${style as CSSResult}
  `

  @property({ type: String, attribute: 'tab-title', reflect: true })
  tabTitle = `New Tab`
  @property({ type: Boolean, reflect: true })
  selected = false

  updated() {
    this.dispatchEvent(new CustomEvent('updated', { bubbles: true }))
  }

  render() {
    return html`
      <div class="dig-ui-tab ${classMap({ selected: this.selected })}">
        <slot></slot>
      </div>
    `
  }
}
