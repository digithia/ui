import {
  customElement,
  LitElement,
  css,
  html,
  internalProperty,
  CSSResult,
  query,
} from 'lit-element'
import { classMap } from 'lit-html/directives/class-map'

import sharedStyle from '@src/dig-ui-shared.scss'
import style from './dig-ui-tabset.scss'
import { DigUtils } from '@digithia/util'

enum Animations {
  backwardOut = 'animateBackwardsOut',
  backwardIn = 'animateBackwardsIn',
  forwardOut = 'animateForwardsOut',
  forwardIn = 'animateForwardsIn',
}

@customElement('dig-ui-tabset')
export class DigUiTabset extends LitElement {
  static styles = css`
    ${sharedStyle as CSSResult}
    ${style as CSSResult}
  `

  @internalProperty()
  tabs: Element[] = []
  @internalProperty()
  titles: (string | null)[] = []
  @internalProperty()
  selectedTabIndex: number = 0
  @internalProperty()
  animateClass: string = ''

  @query('slot')
  slotEl!: HTMLSlotElement

  render() {
    return html`
      <div class="dig-ui-tabset">
        <div class="dig-ui-tabset-titles">
          ${this.titles.map((title, i) => {
            return html`
              <div
                class="dig-ui-tabset-title ${classMap({
                  selected: i === this.selectedTabIndex,
                })}"
                @click="${() => {
                  this.changeTab(i)
                }}"
                @keydown="${(e: KeyboardEvent) => {
                  if (e.key === 'Enter') {
                    this.changeTab(i)
                  }
                }}"
                tabindex="${i + 1}"
              >
                ${title}
              </div>
            `
          })}
        </div>
        <div class="dig-ui-tabset-content ${this.animateClass}">
          <slot
            @slotchange="${this.slotChange}"
            @updated="${this.slotChange}"
          ></slot>
        </div>
      </div>
    `
  }

  slotChange() {
    this.tabs = this.slotEl?.assignedElements() || []
    this.titles = this.tabs.map((t) => t.getAttribute('tab-title'))
    this.selectedTabIndex = this.tabs
      .map((t) => t.getAttribute('selected'))
      .reduce((res, selected, index) => {
        if (selected !== null) return index
        return res
      }, 0)
  }

  changeTab(index: number): void {
    const oldTabIndex = this.selectedTabIndex

    if (oldTabIndex === index) {
      return
    }

    if (index > oldTabIndex) {
      this.animateClass = Animations.backwardOut
    } else {
      this.animateClass = Animations.forwardOut
    }

    const cssAnimationDuration = getComputedStyle(this).getPropertyValue(
      '--dig-ui-tabset-animation-duration'
    )
    const animationDuration =
      DigUtils.cssTimeToMilliseconds(cssAnimationDuration)

    this.selectedTabIndex = index
    for (const tab of this.tabs) {
      tab.removeAttribute('selected')
    }
    this.tabs[index].setAttribute('selected', '')

    if (index > oldTabIndex) {
      this.animateClass = Animations.backwardIn
    } else {
      this.animateClass = Animations.forwardIn
    }

    setTimeout(() => {
      this.animateClass = ''
    }, animationDuration)

    this.dispatchEvent(
      new CustomEvent('select', { detail: this.selectedTabIndex })
    )
  }
}
