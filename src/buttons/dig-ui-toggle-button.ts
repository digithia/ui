import { LitElement, property } from 'lit-element'

export abstract class DigUiToggleButton extends LitElement {
  @property({
    type: Boolean,
    converter: {
      fromAttribute: (attr: string) => {
        if (attr === 'false') return false
        return !!attr
      },
    },
  })
  toggled = false

  toggle(): void {
    this.toggled = !this.toggled
    const event = new CustomEvent('toggle', { detail: this.toggled })
    this.dispatchEvent(event)
  }
}
