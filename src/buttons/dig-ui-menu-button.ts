import { customElement, css, html, CSSResult, property } from 'lit-element'
import { classMap } from 'lit-html/directives/class-map'
import { DigUiToggleButton } from './dig-ui-toggle-button'

import sharedStyle from '@src/dig-ui-shared.scss'
import style from './dig-ui-menu-button.scss'

@customElement('dig-ui-menu-button')
export class DigUiMenuButton extends DigUiToggleButton {
  static styles = css`
    ${sharedStyle as CSSResult}
    ${style as CSSResult}
  `

  @property({ type: Boolean })
  toggled = false

  constructor() {
    super()
    this.addEventListener('click', this.toggle)
  }

  render() {
    return html`
      <div class="menu-button ${classMap({ toggled: this.toggled })}"></div>
    `
  }
}
