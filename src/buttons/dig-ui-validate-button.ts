import { customElement, css, html, CSSResult } from 'lit-element'
import { classMap } from 'lit-html/directives/class-map'
import { DigUiToggleButton } from './dig-ui-toggle-button'

import sharedStyle from '@src/dig-ui-shared.scss'
import style from './dig-ui-validate-button.scss'

@customElement('dig-ui-validate-button')
export class DigUiValidateButton extends DigUiToggleButton {
  static styles = css`
    ${sharedStyle as CSSResult}
    ${style as CSSResult}
  `

  constructor() {
    super()
    this.addEventListener('click', this.toggle)
  }

  render() {
    return html`
      <div class="icon ${classMap({ toggled: this.toggled })}"></div>
    `
  }
}
