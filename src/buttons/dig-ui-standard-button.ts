import { DigUtils } from '@digithia/util'
import {
  customElement,
  LitElement,
  css,
  property,
  html,
  CSSResult,
  query,
} from 'lit-element'

import sharedStyle from '@src/dig-ui-shared.scss'
import style from './dig-ui-standard-button.scss'

@customElement('dig-ui-standard-button')
export class DigUiStandardButton extends LitElement {
  static styles = css`
    ${sharedStyle as CSSResult}
    ${style as CSSResult}
  `

  @property({ type: Boolean })
  disabled = false

  @property({ type: Boolean })
  waves = true

  constructor() {
    super()
  }

  @query('button')
  buttonEl!: HTMLButtonElement

  render() {
    return html`
      <button ?disabled="${this.disabled}" @click="${this.onClick}">
        <slot></slot>
      </button>
    `
  }

  onClick(event: MouseEvent) {
    this.trigger(event.clientX, event.clientY)
  }

  trigger(x: number, y: number) {
    this.dispatchEvent(new CustomEvent('trigger'))
    if (this.waves) {
      this.triggerWaves(
        x || this.buttonEl.clientWidth / 2 + this.buttonEl.offsetLeft,
        y || this.buttonEl.clientHeight / 2 + this.buttonEl.offsetTop
      )
    }
  }

  protected triggerWaves = (x: number, y: number) => {
    const button = this.buttonEl
    const ripple = document.createElement('div')
    ripple.classList.add('ripple')
    const diameter = Math.max(button.clientWidth, button.clientHeight)
    const offset = button.getBoundingClientRect()
    ripple.style.height = diameter + 'px'
    ripple.style.width = diameter + 'px'
    ripple.style.left = x - offset.left - diameter / 2 + 'px'
    ripple.style.top = y - offset.top - diameter / 2 + 'px'

    button.appendChild(ripple)

    const animationDuration = getComputedStyle(
      this.shadowRoot?.host as HTMLElement
    ).getPropertyValue('--dig-ui-standard-button-wave-duration')

    setTimeout(() => {
      button.removeChild(ripple)
    }, DigUtils.cssTimeToMilliseconds(animationDuration))
  }
}
