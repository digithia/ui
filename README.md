# Digithia Ui

This library provides ready to use ui components.

[![pipeline status](https://gitlab.com/digithia/ui/badges/master/pipeline.svg)](https://gitlab.com/digithia/ui/commits/master)
[![coverage report](https://gitlab.com/digithia/ui/badges/master/coverage.svg)](https://gitlab.com/digithia/ui/commits/master)

## Install

run `npm install @digithia/ui` in a project.
