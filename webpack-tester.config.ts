import { Configuration } from 'webpack'
import path from 'path'
import HtmlWebpackPlugin from 'html-webpack-plugin'
import { merge } from 'webpack-merge'
import commonConfig from './webpack-common.config'

const testFiles = ['index', 'buttons', 'pagination', 'login', 'popup', 'test']

const config: Configuration = merge(commonConfig, {
  watch: true,
  mode: 'development',
  devtool: 'source-map',
  entry: {},
  output: {
    path: path.resolve(__dirname, './e2e/dist/'),
    filename: '[name].js',
  },
  plugins: [],
})

for (const file of testFiles) {
  ;(config.entry as any)[file] = path.resolve(
    __dirname,
    `./e2e/tester/${file}.ts`
  )

  config.plugins?.push(
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, `./e2e/tester/${file}.html`),
      inject: true,
      chunks: [file],
      filename: `${file}.html`,
    })
  )
}

export default config
