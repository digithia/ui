import '@src/index'
import { DigUiPagination } from '@src/index'

const paginationEl = document.querySelector('#pagination') as DigUiPagination

paginationEl?.addEventListener('change', (event) => {
  paginationEl.currentPage = (event as CustomEvent).detail
})

setTimeout(() => {
  paginationEl.setAttribute('current-page', '' + Math.floor(Math.random() * 15))
}, 2000)
