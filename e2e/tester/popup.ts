import '@src/popup'
import '@src/buttons'
import { DigUiPopup } from '@src/popup'

const btnDefault = document.querySelector('#btn-default')
const btnRequired = document.querySelector('#btn-required')
const popupDefault = document.querySelector('#popup-default') as DigUiPopup
const popupRequired = document.querySelector('#popup-required') as DigUiPopup
const cancelDefault = document.querySelector(
  '#btn-default-cancel'
) as DigUiPopup
const cancelRequired = document.querySelector(
  '#btn-required-cancel'
) as DigUiPopup

btnDefault?.addEventListener('click', () => {
  popupDefault.trigger()
})
btnRequired?.addEventListener('click', () => {
  popupRequired.trigger()
})
cancelDefault?.addEventListener('click', () => {
  popupDefault.close()
  popupRequired.close()
})
cancelRequired?.addEventListener('click', () => {
  popupDefault.close()
  popupRequired.close()
})
