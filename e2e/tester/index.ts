import { DigUiToggleButton } from '@src/buttons/dig-ui-toggle-button'
import '@src/index'
import { DigUiPopup, DigUiTab, DigUiView } from '@src/index'

const standardButton = document.querySelector('dig-ui-standard-button')

standardButton?.addEventListener('click', () => {
  const message = document.createElement('span')
  message.innerText = Math.random() > 0.5 ? `I'm a toast` : `I'm a bigger toast`
  const toast = document.createElement('dig-ui-toast')
  const rnd = Math.random()
  toast.setAttribute(
    'type',
    rnd < 0.75
      ? rnd < 0.5
        ? rnd < 0.25
          ? 'info'
          : 'warning'
        : 'error'
      : 'success'
  )
  toast.setAttribute('lifespan', '7000')
  toast.append(message)
  document.querySelector('dig-ui-toaster')?.prepend(toast)
})

const editButton = document.querySelector('#edit-button')

editButton?.addEventListener('click', () => {
  const editCard = document.querySelector('dig-ui-edit-card')

  editCard?.removeAttribute('toggled')
})

// const button = document.querySelector('#nav-bar-button') as HTMLButtonElement
// const view = document.querySelector('dig-ui-view') as HTMLElement
// button.addEventListener('toggle', (e: any) => {
//   if (e.detail) {
//     view.setAttribute('toggled', '')
//   } else {
//     view.removeAttribute('toggled')
//   }
// })

const buttonDisabled = document.querySelector(
  '#btn-disabled'
) as HTMLButtonElement
const buttonDisabledCtrl = document.querySelector(
  '#btn-disabled-ctrl'
) as HTMLButtonElement
const buttonTabName = document.querySelector(
  '#btn-tab-name'
) as HTMLButtonElement
buttonTabName.addEventListener('click', (e: any) => {
  const tab = document.querySelector('dig-ui-tab') as DigUiTab
  tab.tabTitle = 'Title changed'
})

buttonDisabledCtrl.addEventListener('trigger', () => {
  if (buttonDisabled.disabled) {
    buttonDisabled.removeAttribute('disabled')
  } else {
    buttonDisabled.setAttribute('disabled', '')
  }
})

setTimeout(() => {
  const view = document.querySelector('#view') as DigUiView
  const el = document.querySelector('#side-bar-change') as HTMLElement
  el.innerHTML = 'Nouveau truc trop trop long'

  view.refreshSideBar()
}, 1000)

// setTimeout(() => {
//   const el = document.querySelector('#side-bar-change') as HTMLElement
//   el?.remove()
// }, 4000)

// setTimeout(() => {
//   const el = document.querySelector('.side-bar') as HTMLElement
//   el?.remove()
// }, 5000)

const tabset = document.querySelector('dig-ui-tabset')
tabset?.addEventListener('select', () => {
  console.log('ok')
})

const navBarToggle = document.querySelector('.nav-bar-toggle')
navBarToggle?.addEventListener('click', () => {
  const div = document.createElement('div')
  div.innerHTML = `lorem \nipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor `
  div.style.position = 'absolute'
  div.style.height = '100px'
  div.style.width = '100px'
  div.style.backgroundColor = 'red'
  navBarToggle.appendChild(div)
})

const btnDefault = document.querySelector('#btn-default')
const popupDefault = document.querySelector('#popup-default') as DigUiPopup
const cancelDefault = document.querySelector(
  '#btn-default-cancel'
) as DigUiPopup

btnDefault?.addEventListener('click', () => {
  popupDefault.trigger()
})
cancelDefault?.addEventListener('click', () => {
  popupDefault.close()
})
